package com.gradproject.bris.dhs.googleglassgradproject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jeremywen on 1/09/2015.
 */
public class MasterProgram {

    private String name;
    private List<Flow> flows;
    private int selectedIndex;
    private int selectedPage;
    private List<FlowGroup> flowGroupList;

    public MasterProgram(String name, List<Flow> flows){
        this.name = name;
        this.flows = flows;

        selectedIndex = 0;
        selectedPage = 0;

        flowGroupList = new ArrayList<>();
        int idx = 0;
        for(int i = 0; i < getNumPages(); i++){
            FlowGroup fg = new FlowGroup();
            for(int j = 0; j < 4; j++){
                if(idx == flows.size()){
                    break;
                }
                fg.add(flows.get(idx));
                idx++;
            }
            flowGroupList.add(fg);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Flow> getFlows() {
        return flows;
    }

    public void setFlows(List<Flow> flows) {
        this.flows = flows;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public int getSelectedPage() {
        return selectedPage;
    }

    public void setSelectedPage(int selectedPage) {
        this.selectedPage = selectedPage;
    }

    public List<FlowGroup> getFlowGroupList() {
        return flowGroupList;
    }

    public void setFlowGroupList(List<FlowGroup> flowGroupList) {
        this.flowGroupList = flowGroupList;
    }

    public int getNumPages(){
        int numPages = (int) (Math.ceil((double) flows.size() / 4.0));
        if(numPages == 0) numPages = 1;           // No Flows
        return numPages;
    }

    public int getNumFlowsOnPage(){
        return flowGroupList.get(selectedPage).getNumFlows();
    }
}
