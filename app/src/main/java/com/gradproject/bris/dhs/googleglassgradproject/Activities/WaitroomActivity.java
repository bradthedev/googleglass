package com.gradproject.bris.dhs.googleglassgradproject.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.glass.media.Sounds;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.google.android.glass.view.WindowUtils;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;
import com.gradproject.bris.dhs.googleglassgradproject.Constants;
import com.gradproject.bris.dhs.googleglassgradproject.Customer;
import com.gradproject.bris.dhs.googleglassgradproject.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class WaitroomActivity extends JabberwockyActivity {

    private CardScrollView mCardScroller;

    private GestureDetector mGestureDetector;

    private View mView;

    private ScrollView waitroomSV;

    private TableLayout waitroomTL;

    public static ArrayList<Customer> customerList = new ArrayList<Customer>();

    public WaitroomActivity(){

    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);
        mGestureDetector = createGestureDetector(this);

        mView = super.getView(getLayoutInflater().inflate(R.layout.waitroom_layout, null));

        mCardScroller = new CardScrollView(this);
        mCardScroller.setAdapter(new CardScrollAdapter() {
            @Override
            public int getCount() {
                return 1;
            }

            @Override
            public Object getItem(int position) {
                return mView;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return mView;
            }

            @Override
            public int getPosition(Object item) {
                if (mView.equals(item)) {
                    return 0;
                }
                return AdapterView.INVALID_POSITION;
            }
        });
        // Handle the TAP event.
        mCardScroller.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                am.playSoundEffect(Sounds.TAP);
                onAddCustomer();
            }

        });
        setContentView(mCardScroller);

        initViews();
        updateView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCardScroller.activate();
    }

    @Override
    protected void onPause() {
        mCardScroller.deactivate();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == Constants.SPEECH_REQUEST && resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String spokenText = results.get(0);
            if(spokenText == null) {
                spokenText = "";
            }
            Customer cust = new Customer(spokenText);
            cust.getTimerHandler().postDelayed(cust.getTimerRunnable(), 0);
            addCustomer(cust);
            updateView();
        }
        //super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event){
        if(mGestureDetector != null){
            return mGestureDetector.onMotionEvent(event);
        }
        return false;
    }

    private GestureDetector createGestureDetector(Context context){
        GestureDetector gestureDetector = new GestureDetector(context);

        gestureDetector.setBaseListener(new GestureDetector.BaseListener() {
            @Override
            public boolean onGesture(Gesture gesture) {
                if (gesture == Gesture.SWIPE_RIGHT) {
                    rightSwipe();
                    return true;
                } else if (gesture == Gesture.SWIPE_LEFT) {
                    leftSwipe();
                    return true;
                } else if (gesture == Gesture.SWIPE_DOWN){
                    downSwipe();
                } else if (gesture == Gesture.TWO_TAP){
                    Customer customer = new Customer("Bobby");
                    customer.getTimerHandler().postDelayed(customer.getTimerRunnable(), 0);
                    addCustomer(customer);
                    updateView();
                } else if (gesture == Gesture.THREE_TAP) {
                    if (popupDisplay) {
                        dismissPopup();
                    } else {
                        displayPopup(1);
                    }
                    return true;
                }

                return false;
            }
        });
        return gestureDetector;
    }

    private void initViews(){
        waitroomSV = (ScrollView) mView.findViewById(R.id.waitroom_sv);
        waitroomTL = (TableLayout) mView.findViewById(R.id.waitroom_tl);
    }

    private void rightSwipe(){
        mCardScroller.getAdapter().notifyDataSetChanged();
    }

    private void leftSwipe(){
        mCardScroller.getAdapter().notifyDataSetChanged();
    }

    private void downSwipe(){
        Intent intent = new Intent(this, LandingPageActivity.class);
        startActivity(intent);
        finish();
    }

    private void onAddCustomer(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        startActivityForResult(intent, Constants.SPEECH_REQUEST);
    }

    private void addCustomer(Customer c){
        customerList.add(c);
    }

    private void updateView(){
        TextView name_tv;
        TextView time_tv;
        TableRow tableRow;
        waitroomTL.removeAllViews();
        for(int i = 0; i < customerList.size(); i++){
            name_tv = new TextView(this, null, R.style.Text);
            time_tv = new TextView(this, null, R.style.Text);
            name_tv.setTextColor(Color.WHITE);
            time_tv.setTextColor(Color.WHITE);
            time_tv.setGravity(Gravity.RIGHT);
            customerList.get(i).setTextView(time_tv);
            name_tv.setText(customerList.get(i).getName());
            time_tv.setText(customerList.get(i).getTimeStamp());
            tableRow = new TableRow(this);
            tableRow.addView(name_tv);
            tableRow.addView(time_tv);
            waitroomTL.addView(tableRow);
        }
        mCardScroller.getAdapter().notifyDataSetChanged();
    }

}
