package com.gradproject.bris.dhs.googleglassgradproject.Activities;

import com.google.android.glass.content.Intents;
import com.google.android.glass.media.Sounds;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.google.android.glass.view.WindowUtils;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;
import com.gradproject.bris.dhs.googleglassgradproject.Constants;
import com.gradproject.bris.dhs.googleglassgradproject.HttpConnection;
import com.gradproject.bris.dhs.googleglassgradproject.R;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;

import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LandingPageActivity extends JabberwockyActivity {

    private CardScrollView mCardScroller;

    private GestureDetector mGestureDetector;

    private View mView;

    private int selectedIndex = 0;

    private TextView headingTV, gpTV, msgTV, settingsTV, msgCountTV, videoTV, waitroomTV;

    private List<TextView> moduleTVs;

    public LandingPageActivity(){

    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);
        mGestureDetector = createGestureDetector(this);

        mView = super.getView(getLayoutInflater().inflate(R.layout.menus_home_layout, null));

        mCardScroller = new CardScrollView(this);
        mCardScroller.setAdapter(new CardScrollAdapter() {
            @Override
            public int getCount() {
                return 1;
            }

            @Override
            public Object getItem(int position) {
                return mView;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return mView;
            }

            @Override
            public int getPosition(Object item) {
                if (mView.equals(item)) {
                    return 0;
                }
                return AdapterView.INVALID_POSITION;
            }
        });
        // Handle the TAP event.
        mCardScroller.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                am.playSoundEffect(Sounds.TAP);
                chooseService(selectedIndex);
            }

        });
        setContentView(mCardScroller);

        initViews();
        setSelectedService(selectedIndex);
        //initCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCardScroller.activate();
    }

    @Override
    protected void onPause() {
        mCardScroller.deactivate();
        super.onPause();
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event){
        if(mGestureDetector != null){
            return mGestureDetector.onMotionEvent(event);
        }
        return false;
    }

    private GestureDetector createGestureDetector(Context context){
        GestureDetector gestureDetector = new GestureDetector(context);

        gestureDetector.setBaseListener(new GestureDetector.BaseListener() {
            @Override
            public boolean onGesture(Gesture gesture) {
                if (gesture == Gesture.SWIPE_RIGHT) {
                    rightSwipe();
                    return true;
                } else if (gesture == Gesture.SWIPE_LEFT) {
                    leftSwipe();
                    return true;
                } else if (gesture == Gesture.TWO_TAP) {
                    if (popupDisplay) {
                        dismissPopup();
                    } else {
                        displayPopup(1);
                    }
                    return true;
                }

                return false;
            }
        });
        return gestureDetector;
    }

    private void initViews(){

        headingTV = (TextView) mView.findViewById(R.id.heading_tv);
        gpTV = (TextView) mView.findViewById(R.id.gp_tv);
        msgTV = (TextView) mView.findViewById(R.id.msg_tv);
        settingsTV = (TextView) mView.findViewById(R.id.settings_tv);
        videoTV = (TextView) mView.findViewById(R.id.vid_tv);
        waitroomTV = (TextView) mView.findViewById(R.id.waitroom_tv);

        moduleTVs = new ArrayList<TextView>();
        moduleTVs.add(gpTV);
        moduleTVs.add(msgTV);
        moduleTVs.add(waitroomTV);
        settingsTV.setVisibility(View.INVISIBLE);
        //moduleTVs.add(settingsTV);
        if(savedVideoIntent == null){
            videoTV.setVisibility(View.INVISIBLE);
        }else{
            videoTV.setVisibility(View.VISIBLE);
            moduleTVs.add(videoTV);
        }

        //loadMessages();
        msgCountTV = (TextView) mView.findViewById(R.id.msg_count_tv);
        msgCountTV.setText(getUnreadMessages() + "");


        //HttpConnection http = new HttpConnection();
        //headingTV.setText(http.sendRequest());

    }

    private void setSelectedService(int serviceId){
        for(int i = 0; i < moduleTVs.size(); i++){
            LinearLayout parentTV = (LinearLayout) moduleTVs.get(i).getParent();
            if(serviceId == i){
                parentTV.setBackgroundColor(getResources().getColor(R.color.selected));
                //moduleTVs.get(i).setBackgroundColor(getResources().getColor(R.color.selected));
            }
            else{
                parentTV.setBackgroundColor(Color.TRANSPARENT);
                //moduleTVs.get(i).setBackgroundColor(Color.TRANSPARENT);
            }
        }
    }

    private void chooseService(int serviceId){
        if(serviceId == 0){ //Guided Procedure
            Intent intent = new Intent(this, GuidedProcedureActivity.class);
            startActivity(intent);
            finish();
        }
        else if(serviceId == 1){ //Messaging
            Intent intent = new Intent(this, MessagingActivity.class);
            startActivity(intent);
            finish();
        }
        else if(serviceId == 2){ //Waitroom
            Intent intent = new Intent(this, WaitroomActivity.class);
            startActivity(intent);
            finish();
        }
        else if(serviceId == 3){ //Video
            startActivity(savedVideoIntent);
        }
    }

    private void rightSwipe(){
        selectedIndex++;
        if(selectedIndex >= moduleTVs.size()){
            selectedIndex = 0;
        }
        setSelectedService(selectedIndex);
        mCardScroller.getAdapter().notifyDataSetChanged();
    }

    private void leftSwipe(){
        selectedIndex--;
        if(selectedIndex < 0){
            selectedIndex = moduleTVs.size() - 1;
        }
        setSelectedService(selectedIndex);
        mCardScroller.getAdapter().notifyDataSetChanged();
    }

}
