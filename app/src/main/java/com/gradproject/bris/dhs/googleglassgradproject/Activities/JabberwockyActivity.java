package com.gradproject.bris.dhs.googleglassgradproject.Activities;

import com.google.android.glass.content.Intents;
import com.google.android.glass.widget.CardScrollView;
import com.gradproject.bris.dhs.googleglassgradproject.Message;
import com.gradproject.bris.dhs.googleglassgradproject.MessageGroup;
import com.gradproject.bris.dhs.googleglassgradproject.R;

import android.app.Activity;
import android.content.Intent;
import android.os.FileObserver;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class JabberwockyActivity extends Activity {

    private static boolean colourBlindActive = false;
    private static boolean highContrastActive = false;
    private static boolean trainingModeActive = true;
    public static boolean[] settingsOptions = {colourBlindActive, highContrastActive, trainingModeActive};

    public static Intent savedVideoIntent;

    /* Messaging */
    public static List<Message> messagesTest = new ArrayList<>();   // Hard coded data
    public static List<Message> messages = new ArrayList<>();
    public static ArrayList<MessageGroup> messageGroups = new ArrayList<>();
    //////////////

    public ImageView cameraIV;
    public boolean cameraVisible = false;

    public boolean popupDisplay = false;

    private View jabberwockyView;

    private View contentView;

    private FrameLayout contentLayout;

    public JabberwockyActivity(){
        loadMessages();
        messageGroups = getMessageGroups();
    }

    private void initView(View insertView){
        jabberwockyView = getLayoutInflater().inflate(R.layout.global_layout, null);

        cameraIV = (ImageView) jabberwockyView.findViewById(R.id.global_camera_iv);
        if(cameraVisible == true){
            cameraIV.setVisibility(View.VISIBLE);
        }
        else{
            cameraIV.setVisibility(View.INVISIBLE);
        }

        contentLayout = (FrameLayout) jabberwockyView.findViewById(R.id.contentFrame);
        contentLayout.removeAllViews();

        contentView = insertView;
        contentLayout.addView(contentView);

    }

    public View getView(View insertView){
        initView(insertView);
        return jabberwockyView;
    }

    public void displayPopup(int i){        // 0 = msg, 1 = rsa
        popupDisplay = true;
        if(i == 0){
            contentLayout.addView(getLayoutInflater().inflate(R.layout.popmsg_overlay_layout, null));
        }
        else if (i == 1){
            contentLayout.addView(getLayoutInflater().inflate(R.layout.poprsa_overlay_layout, null));
        }
    }
    public void displayPopupMessage(Message msg){
        popupDisplay = true;
        //View msgView = getLayoutInflater().inflate(R.layout.popmsg_overlay_layout, null);
        contentLayout.addView(getLayoutInflater().inflate(R.layout.popmsg_overlay_layout, null));
        TextView senderTV = (TextView) findViewById(R.id.popup_sender_tv);
        TextView messageTV = (TextView) findViewById(R.id.popup_message_tv);
        senderTV.setText(msg.getSender() + " (" + msg.getTime() + ")");
        messageTV.setText(msg.getMessage());
    }
    public void dismissPopup(){
        popupDisplay = false;
        contentLayout.removeAllViews();
        contentLayout.addView(contentView);
    }


    @Override
    public boolean onKeyDown(int keycode, KeyEvent event){
        if(keycode == KeyEvent.KEYCODE_CAMERA){
            onCameraKeyDown();
            return true;
        }
        return super.onKeyDown(keycode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2 && resultCode == RESULT_OK) {
            String thumbnailPath = data.getStringExtra(Intents.EXTRA_THUMBNAIL_FILE_PATH);
            String videoPath = data.getStringExtra(Intents.EXTRA_VIDEO_FILE_PATH);
            Log.e("VIDEOPATH", videoPath);
            processVideoWhenReady(videoPath);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /* Duress Alarm + Video Recording */
    public void onCameraKeyDown(){

        /*if(isRecording == false){
            //Release Camera before MediaRecorder start
            releaseCamera();

            if(!prepareMediaRecorder()){

                finish();
            }

            mediaRecorder.start();
            isRecording = true;
        }
        else{
            // stop recording and release camera
            mediaRecorder.stop();  // stop the recording
            releaseMediaRecorder(); // release the MediaRecorder object
            isRecording = false;
            //Exit after saved
            finish();
        }
        if(isRecording){
            headingTV.setText("Is Currently Recording");
        }*/

        /*  // THIS IS THE FULL SCREEN INTENT VIDEO CAPTURE
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(intent, 2);
        */

        Intent videoIntent = new Intent(this, VideoCaptureActivity.class);
        startActivity(videoIntent);

        //sendLyncMessage();
        /*
        if(cameraVisible == false) {
            cameraIV.setVisibility(View.VISIBLE);
            cameraVisible = true;
        }
        else{
            cameraIV.setVisibility(View.INVISIBLE);
            cameraVisible = false;
        }*/
    }

    private void sendLyncMessage(){
        String url = "http://10.245.32.33:2016/api/SendLyncMessage?domain=internal&username=rjw906&password=2535743380Sj@!&fromSip=jeremy.wen@humanservices.gov.au&toSip=jeremy.wen@humanservices.gov.au&subject=alert&message=thitest";
        try {
            Log.e("MY TAG", "STEP 1");
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();        // Not sure if I can make this global and perhaps use disconnect()
            Log.e("MY TAG", "STEP 1");
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");

            int responseCode = con.getResponseCode();
            Log.e("MY TAG", "STEP 2");
            Toast.makeText(getApplicationContext(), "Response Code : " + responseCode, Toast.LENGTH_LONG).show();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Log.e("MY TAG", "STEP 3");
            //print result
            Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processVideoWhenReady(final String picturePath) {
        final File pictureFile = new File(picturePath);

        if (pictureFile.exists()) {
            // The picture is ready; process it.
        } else {
            // The file does not exist yet. Before starting the file observer, you
            // can update your UI to let the user know that the application is
            // waiting for the picture (for example, by displaying the thumbnail
            // image and a progress indicator).

            final File parentDirectory = pictureFile.getParentFile();
            FileObserver observer = new FileObserver(parentDirectory.getPath(),
                    FileObserver.CLOSE_WRITE | FileObserver.MOVED_TO) {
                // Protect against additional pending events after CLOSE_WRITE
                // or MOVED_TO is handled.
                private boolean isFileWritten;

                @Override
                public void onEvent(int event, String path) {
                    if (!isFileWritten) {
                        // For safety, make sure that the file that was created in
                        // the directory is actually the one that we're expecting.
                        File affectedFile = new File(parentDirectory, path);
                        isFileWritten = affectedFile.equals(pictureFile);

                        if (isFileWritten) {
                            stopWatching();

                            // Now that the file is ready, recursively call
                            // processPictureWhenReady again (on the UI thread).
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    processVideoWhenReady(picturePath);
                                }
                            });
                        }
                    }
                }
            };
            observer.startWatching();
        }
    }

    /* Hardcoded test method delete once Skype is working */
    public static void loadMessages(){
        messagesTest.clear();
        Message msg = new Message("Toni Abort", "3:30pm", "Sorry I missed the flight, can you help me rebook?");
        Message msg1 = new Message("Toni Abort", "8:00am", "I'm going Vegas for a conference, please make sure my TA is sent on time.");
        Message msg2 = new Message("Josh Boss", "11:44am", "Reminder, meeting at 2");

        messagesTest.add(msg);
        messagesTest.add(msg1);
        messagesTest.add(msg2);

        messages = messagesTest;
    }

    public static int getUnreadMessages(){
        int unreadMessages = 0;
        for(int i = 0; i < messageGroups.size(); i++){
            for(int j = 0; j < messageGroups.get(i).getMessages().size(); j++) {
                if (messageGroups.get(i).getMessages().get(j).isRead() == false) {
                    unreadMessages++;
                }
            }
        }
        return unreadMessages;
    }

    private ArrayList<MessageGroup> getMessageGroups(){
        ArrayList<MessageGroup> messageGroups = new ArrayList<>();
        ArrayList<String> senderNames = new ArrayList<>();
        for(int i = 0; i < messages.size(); i++){
            if(!senderNames.contains(messages.get(i).getSender())){
                senderNames.add(messages.get(i).getSender());
            }
        }
        for(int i = 0; i < senderNames.size(); i++){
            messageGroups.add(new MessageGroup(senderNames.get(i)));
        }
        for(int i = 0; i < messages.size(); i++){
            for(int j = 0; j < messageGroups.size(); j++){
                if(messageGroups.get(j).getName().equals(messages.get(i).getSender())){
                    messageGroups.get(j).getMessages().add(messages.get(i));
                }
            }
        }
        return messageGroups;
    }
}
