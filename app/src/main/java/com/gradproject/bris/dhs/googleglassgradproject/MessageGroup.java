package com.gradproject.bris.dhs.googleglassgradproject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jeremywen on 11/09/2015.
 */
public class MessageGroup{

    String name;
    List<Message> messages;

    public MessageGroup(String name){
        this.name = name;
        messages = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
