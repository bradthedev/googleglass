package com.gradproject.bris.dhs.googleglassgradproject.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.glass.media.Sounds;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.google.android.glass.view.WindowUtils;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;
import com.gradproject.bris.dhs.googleglassgradproject.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class VideoCaptureActivity extends JabberwockyActivity {

    TextView mTimerInfo;
    SurfaceView mPreview;
    Camera mCamera;
    SurfaceHolder mPreviewHolder;

    private CardScrollView mCardScroller;
    private GestureDetector mGestureDetector;
    private View mView;

    File mOutputFile;

    private static final int MEDIA_TYPE_IMAGE = 0;
    private static final int MEDIA_TYPE_VIDEO = 1;

    MediaRecorder mrec;

    long second = 0;

    boolean mCameraConfigured;
    boolean mRecording;
    boolean mInPreview;

    SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
        public void surfaceCreated( SurfaceHolder holder ) {
            initPreview();
        }
        public void surfaceChanged( SurfaceHolder holder, int format, int width, int height ) {
            startPreview();
        }
        public void surfaceDestroyed( SurfaceHolder holder ) {
            releaseCamera();
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);
        mGestureDetector = createGestureDetector(this);

        mView = super.getView(getLayoutInflater().inflate(R.layout.video_capture, null));

        mCardScroller = new CardScrollView(this);
        mCardScroller.setAdapter(new CardScrollAdapter() {
            @Override
            public int getCount() {
                return 1;
            }

            @Override
            public Object getItem(int position) {
                return mView;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return mView;
            }

            @Override
            public int getPosition(Object item) {
                if (mView.equals(item)) {
                    return 0;
                }
                return AdapterView.INVALID_POSITION;
            }
        });
        // Handle the TAP event.
        mCardScroller.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                am.playSoundEffect(Sounds.TAP);
                if(mRecording){
                    stopAndPlayVideo();
                }else{
                    startRecord();
                }
            }

        });
        setContentView(mCardScroller);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mTimerInfo = (TextView) mView.findViewById(R.id.timer);
        mTimerInfo.setText("Tap to Record");
        mPreview = (SurfaceView) mView.findViewById(R.id.preview);
        mPreviewHolder = mPreview.getHolder();
        mPreviewHolder.addCallback(surfaceCallback);
        mCamera = getCameraInstance();
    }

    private void initPreview() {
        if ( mCamera != null && mPreviewHolder.getSurface() != null) {
            try {
                mCamera.setPreviewDisplay(mPreviewHolder);
            }
            catch (IOException e) {
                Toast.makeText(VideoCaptureActivity.this, e.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
            if ( !mCameraConfigured ) {
                Camera.Parameters parameters = mCamera.getParameters();
                parameters.setPreviewFpsRange(30000, 30000);
                parameters.setPreviewSize(640, 360);
                mCamera.setParameters(parameters);
                mCameraConfigured = true;
            }
        }
    }

    private void startPreview() {
        if ( mCameraConfigured && mCamera != null ) {
            mCamera.startPreview();
            mInPreview = true;
        }
    }

    private final Handler mHandler = new Handler();
    private final Runnable mUpdateTextRunnable = new Runnable() {
        @Override
        public void run() {
            if (mRecording) {
                mTimerInfo.setText(String.format("%02d:%02d:%02d",
                        TimeUnit.MILLISECONDS.toHours(second*1000),
                        TimeUnit.MILLISECONDS.toMinutes(second*1000) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(second*1000)),
                        TimeUnit.MILLISECONDS.toSeconds(second*1000) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(second*1000))));
                second++; }
            mHandler.postDelayed(mUpdateTextRunnable, 1000);
        }
    };

    private void startRecord(){
        if (mRecording)
            return;
        if (prepareVideoRecorder()) {
            mrec.start();
            mRecording = true;
            second = 0;
            mHandler.post(mUpdateTextRunnable);
        } else {
            releaseMediaRecorder();
        }
    }

    void stopAndPlayVideo() {
        stopRecording();
        mTimerInfo.setText("Recording Done");
        //Toast.makeText(CustomVideoCaptureActivity.this, "Video saved to " + mOutputFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        savedVideoIntent = new Intent();
        savedVideoIntent.setAction("com.google.glass.action.VIDEOPLAYER");
        savedVideoIntent.putExtra("video_url", mOutputFile.getAbsolutePath());
        try {
            Toast.makeText(VideoCaptureActivity.this, mOutputFile.mkdir() + " - " + mOutputFile.createNewFile() + " - " + mOutputFile.toString(), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(VideoCaptureActivity.this, "Your life is a FAILURE", Toast.LENGTH_LONG).show();
        }
        exitVideo();
        //startActivity(savedVideo);
    }

    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
        }
        catch (Exception e){
            Log.e("MYTAG", e.getMessage());
        }
        return c;
    }

    private File getOutputMediaFile(int type){
        File mediaStorageDir = new File("/storage/emulated/0/DCIM/Camera/");
        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Toast.makeText(VideoCaptureActivity.this, "Failed to create directory", Toast.LENGTH_LONG).show();
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath(), "IMG_"+ timeStamp + ".jpg");
        }
        else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath(), "VID_"+ timeStamp + ".mp4");
        }
        else return null;
        return mediaFile;
    }

    private boolean prepareVideoRecorder(){
        if (mCamera != null){
            mCamera.release(); // release the camera for other applications
        }
        mCamera = getCameraInstance();
        if (mCamera == null) return false;
        mrec = new MediaRecorder();
        mCamera.unlock();
        mrec.setCamera(mCamera);
        mrec.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mrec.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mrec.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
        mrec.setPreviewDisplay(mPreviewHolder.getSurface());
        mOutputFile = getOutputMediaFile(MEDIA_TYPE_VIDEO);
        mrec.setOutputFile(mOutputFile.toString());
        try {
            mrec.prepare();
        }
        catch (Exception e) {
            return false;
        }
        return true;
    }


    @Override
    protected void onPause() {
        super.onPause();
        releaseMediaRecorder(); // if you are using MediaRecorder, release it first
        releaseCamera();        // release the camera immediately on pause event
    }
    @Override
    protected void onDestroy() {
        if (mRecording)
            stopAndPlayVideo();
        mRecording = false;
        super.onDestroy();
    }
    private void releaseMediaRecorder(){
        if (mrec != null) {
            mrec.reset();
            mrec.release();
            mrec = null;
        }
    }

    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();
            mCamera = null;
        }
    }
    protected void stopRecording() {
        if(mrec!=null)
        {
            mrec.stop();
            mrec.release();
            mrec = null;
            releaseCamera();
            mRecording = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCardScroller.activate();
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event){
        if(mGestureDetector != null){
            return mGestureDetector.onMotionEvent(event);
        }
        return false;
    }

    private GestureDetector createGestureDetector(Context context){
        GestureDetector gestureDetector = new GestureDetector(context);

        gestureDetector.setBaseListener(new GestureDetector.BaseListener() {
            @Override
            public boolean onGesture(Gesture gesture) {
                if (gesture == Gesture.SWIPE_DOWN) {
                    exitVideo();
                    return true;
                } else if (gesture == Gesture.TWO_TAP){
                    if(popupDisplay){
                        dismissPopup();
                    }else{
                        displayPopup(1);
                    }
                    return true;
                }

                return false;
            }
        });
        return gestureDetector;
    }

    private void exitVideo(){
        Intent intent = new Intent(this, LandingPageActivity.class);
        startActivity(intent);
        finish();
    }
}
