package com.gradproject.bris.dhs.googleglassgradproject;

/**
 * Created by jeremywen on 8/09/2015.
 */
public class Message {

    private String sender;
    private String time;
    private String message;
    private boolean isRead = false;

    public Message(String sender, String time, String message){
        this.sender = sender;
        this.time = time;
        this.message  = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }
}
