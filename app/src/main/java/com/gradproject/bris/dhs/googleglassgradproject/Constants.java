package com.gradproject.bris.dhs.googleglassgradproject;

import android.graphics.Color;

/**
 * Created by Jeremy on 7/28/2015.
 */
public class Constants {

    public static final int SPEECH_REQUEST = 0;

    public static final String MAIN = "M";
    public static final String HOME = "H";
    public static final String INVALID_CHOICE = "N";
    public static final String END_OF_PROCEDURE = "E";

    public static final String GUIDED_PROCEDURE = "Guided Procedure";
    public static final String SETTINGS = "Settings";

    public static final String MEDICARE = "Medicare";
    public static final String CENTRELINK = "Centrelink";
    public static final String ATO = "Australian Tax Office";

}
