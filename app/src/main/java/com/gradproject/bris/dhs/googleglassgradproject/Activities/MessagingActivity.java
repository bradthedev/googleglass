package com.gradproject.bris.dhs.googleglassgradproject.Activities;

import com.google.android.glass.media.Sounds;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.google.android.glass.widget.CardBuilder;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;
import com.gradproject.bris.dhs.googleglassgradproject.Message;
import com.gradproject.bris.dhs.googleglassgradproject.MessageGroup;
import com.gradproject.bris.dhs.googleglassgradproject.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MessagingActivity extends JabberwockyActivity {

    private int selectedIndex = 0;
    private int screenID = 0;       // 0 = history, 1 = message view

    private int currentMessageIndex = 0;

    private List<TableRow> messageRows;

    private TableLayout msgTL;

    private TextView senderTV, timeTV, msgTV;

    private CardScrollView mCardScroller;
    private View mView;
    private GestureDetector mGestureDetector;

    public MessagingActivity(){

    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        mView = super.getView(getLayoutInflater().inflate(R.layout.msg_history_layout, null));

        mGestureDetector = createGestureDetector(this);

        mCardScroller = new CardScrollView(this);
        mCardScroller.setAdapter(new CardScrollAdapter() {
            @Override
            public int getCount() {
                return 1;
            }

            @Override
            public Object getItem(int position) {
                return mView;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return mView;
            }

            @Override
            public int getPosition(Object item) {
                if (mView.equals(item)) {
                    return 0;
                }
                return AdapterView.INVALID_POSITION;
            }
        });
        // Handle the TAP event.
        mCardScroller.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (screenID == 0) {
                    chooseMessage(selectedIndex);
                }
                AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                am.playSoundEffect(Sounds.TAP);
            }
        });
        setContentView(mCardScroller);

        initHistoryView();
        updateView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCardScroller.activate();
    }

    @Override
    protected void onPause() {
        mCardScroller.deactivate();
        super.onPause();
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event){
        if(mGestureDetector != null){
            return mGestureDetector.onMotionEvent(event);
        }
        return false;
    }

    private GestureDetector createGestureDetector(Context context){
        GestureDetector gestureDetector = new GestureDetector(context);

        gestureDetector.setBaseListener(new GestureDetector.BaseListener() {
            @Override
            public boolean onGesture(Gesture gesture) {
                if (gesture == Gesture.SWIPE_RIGHT) {
                    forwardSwipe();
                    return true;
                } else if (gesture == Gesture.SWIPE_LEFT) {
                    backSwipe();
                    return true;
                } else if (gesture == Gesture.SWIPE_DOWN) {
                    downSwipe();
                    return true;
                }
                return false;
            }
        });
        return gestureDetector;
    }

    private void forwardSwipe(){
        if(screenID == 0) {
            selectedIndex++;
            if (selectedIndex >= messageRows.size()) {
                selectedIndex = 0;
            }
        }
        else if(screenID == 1){
            currentMessageIndex++;
            if (currentMessageIndex >= messageGroups.get(selectedIndex).getMessages().size()) {
                currentMessageIndex = 0;
            }
        }
        updateView();
    }

    private void backSwipe(){
        if(screenID == 0) {
            selectedIndex--;
            if (selectedIndex < 0) {
                selectedIndex = messageRows.size() - 1;
            }
        }
        else if(screenID == 1){
            currentMessageIndex--;
            if (currentMessageIndex < 0) {
                currentMessageIndex = messageGroups.get(selectedIndex).getMessages().size() - 1;
            }
        }
        updateView();
    }

    private void downSwipe(){
        if(screenID == 0) {
            Intent intent = new Intent(this, LandingPageActivity.class);
            startActivity(intent);
            finish();
        }
        else if(screenID == 1){
            screenID = 0;
            mView = super.getView(getLayoutInflater().inflate(R.layout.msg_history_layout, null));
            mCardScroller.getAdapter().notifyDataSetChanged();
            initHistoryView();
            updateView();
        }
    }

    private void chooseMessage(int selectedIndex){
        mView = super.getView(getLayoutInflater().inflate(R.layout.msg_view_layout, null));
        mCardScroller.getAdapter().notifyDataSetChanged();
        screenID = 1;
        initMessageView();
        currentMessageIndex = 0;
        viewMessage(currentMessageIndex);

    }

    private void viewMessage(int msgIdx){
        Message msg = messageGroups.get(selectedIndex).getMessages().get(msgIdx);
        messageGroups.get(selectedIndex).getMessages().get(msgIdx).setIsRead(true);
        senderTV.setText(msg.getSender());
        timeTV.setText(msg.getTime());
        msgTV.setText(msg.getMessage());
    }

    private void initHistoryView(){
        msgTL = (TableLayout) mView.findViewById(R.id.msg_tl);
        messageRows = new ArrayList<>();

        loadMessagesIntoView();
    }

    private void initMessageView(){
        senderTV = (TextView) mView.findViewById(R.id.sender_tv);
        timeTV = (TextView) mView.findViewById(R.id.time_tv);
        msgTV = (TextView) mView.findViewById(R.id.msg_tv);
    }

    private void updateView(){
        if(screenID == 0) {
            for (int i = 0; i < messageRows.size(); i++) {
                if (i == selectedIndex) {
                    messageRows.get(i).setBackgroundColor(getResources().getColor(R.color.selected));
                } else {
                    messageRows.get(i).setBackgroundColor(Color.TRANSPARENT);
                }
            }
        }
        else if(screenID == 1){
            viewMessage(currentMessageIndex);
        }
        mCardScroller.getAdapter().notifyDataSetChanged();
    }

    private void loadMessagesIntoView(){
        TextView name_tv;
        TextView time_tv;
        TableRow tableRow;
        for(int i = 0; i < messageGroups.size(); i++){
            name_tv = new TextView(this, null, R.style.Text);
            time_tv = new TextView(this, null, R.style.Text);
            name_tv.setTextColor(Color.WHITE);
            time_tv.setTextColor(Color.WHITE);
            time_tv.setGravity(Gravity.RIGHT);
            name_tv.setText(messageGroups.get(i).getName());
            time_tv.setText(messageGroups.get(i).getMessages().get(0).getTime());
            tableRow = new TableRow(this);
            tableRow.addView(name_tv);
            tableRow.addView(time_tv);
            msgTL.addView(tableRow);
            messageRows.add(tableRow);
        }
    }

}
