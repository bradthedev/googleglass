package com.gradproject.bris.dhs.googleglassgradproject;

import java.util.List;

/**
 * Created by Jeremy on 7/27/2015.
 * The first element in the list represents the name/identifier of the flow
 */
public class Flow {

    private List<ActionEvent> actionEventList;

    private String currentId = "0";

    public Flow(List<ActionEvent> actionEventList){
        this.actionEventList = actionEventList;
    }

    public String getName(){
        return actionEventList.get(0).getTitle();
    }

    public ActionEvent getById(String id){
        for(int i = 0; i < actionEventList.size(); i++){
            if(actionEventList.get(i).getId().equals(id)){
                return actionEventList.get(i);
            }
        }
        return null;
    }

    public List<ActionEvent> getActionEventList() {
        return actionEventList;
    }

    public void setActionEventList(List<ActionEvent> actionEventList) {
        this.actionEventList = actionEventList;
    }

    public String getCurrentId() {
        return currentId;
    }

    public void setCurrentId(String currentId) {
        this.currentId = currentId;
    }
}
