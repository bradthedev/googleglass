package com.gradproject.bris.dhs.googleglassgradproject;

import java.util.List;

/**
 * Created by Jeremy on 7/27/2015.
 */
public class ActionEvent {
    private String id;
    private String title;
    private String parent;
    private String yesChoice;
    private String noChoice;

    public ActionEvent(String id, String title, String parent, String yesChoice, String noChoice) {
        this.id = id;
        this.title = title;
        this.parent = parent;
        this.yesChoice = yesChoice;
        this.noChoice = noChoice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getYesChoice() {
        return yesChoice;
    }

    public void setYesChoice(String yesChoice) {
        this.yesChoice = yesChoice;
    }

    public String getNoChoice() {
        return noChoice;
    }

    public void setNoChoice(String noChoice) {
        this.noChoice = noChoice;
    }

    public boolean hasNoOption(){
        if(!noChoice.equals(Constants.INVALID_CHOICE)){
            return true;
        }
        return false;
    }
}
