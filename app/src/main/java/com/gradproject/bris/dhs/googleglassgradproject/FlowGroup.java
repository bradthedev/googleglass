package com.gradproject.bris.dhs.googleglassgradproject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jeremy on 03-Aug-15.
 */
public class FlowGroup {

    private List<Flow> flows;

    public FlowGroup(){
        this.flows = new ArrayList<>();
    }

    public Flow getFlow(int i){
        return flows.get(i);
    }

    public void add(Flow f){
        flows.add(f);
    }

    public int getNumFlows(){
        return flows.size();
    }

}
