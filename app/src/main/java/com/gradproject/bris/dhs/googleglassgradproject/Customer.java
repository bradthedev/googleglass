package com.gradproject.bris.dhs.googleglassgradproject;

import android.os.Handler;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by jeremywen on 1/10/2015.
 */
public class Customer {

    private String name;
    private long waitTime;
    private String timeStamp = "M:SS";
    private TextView textView;
    private Handler timerHandler = new Handler();
    private Runnable timerRunnable = new Runnable(){

        @Override
        public void run() {
            long millis = System.currentTimeMillis() - waitTime;
            int seconds = (int) (millis / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;
            timeStamp = String.format("%d:%02d", minutes, seconds);
            timerHandler.postDelayed(this, 0);
            textView.setText(timeStamp);
        }
    };

    public Customer(String name){
        this.name = name;
        this.waitTime = System.currentTimeMillis();
        timerHandler.postDelayed(timerRunnable, 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(long waitTime) {
        this.waitTime = waitTime;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Handler getTimerHandler() {
        return timerHandler;
    }

    public void setTimerHandler(Handler timerHandler) {
        this.timerHandler = timerHandler;
    }

    public Runnable getTimerRunnable() {
        return timerRunnable;
    }

    public void setTimerRunnable(Runnable timerRunnable) {
        this.timerRunnable = timerRunnable;
    }

    public TextView getTextView() {
        return textView;
    }

    public void setTextView(TextView textView) {
        this.textView = textView;
    }
}
