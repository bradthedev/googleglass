package com.gradproject.bris.dhs.googleglassgradproject.Activities;

import com.google.android.glass.media.Sounds;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.google.android.glass.widget.CardBuilder;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;
import com.gradproject.bris.dhs.googleglassgradproject.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ScrollView;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends JabberwockyActivity {

    private int selectedIndex = 0;

    private Switch colorBlindSwitch, highContrastSwitch, trainingModeSwitch;

    private List<Switch> switches;

    private CardScrollView mCardScroller;
    private View mView;
    private GestureDetector mGestureDetector;

    private ScrollView scrollView;

    public SettingsActivity(){

    }

    private void initViews(){
        scrollView = (ScrollView) mView.findViewById(R.id.scrollView);

        colorBlindSwitch = (Switch) mView.findViewById(R.id.colourBlindSwitch);
        highContrastSwitch = (Switch) mView.findViewById(R.id.highContrastSwitch);
        trainingModeSwitch = (Switch) mView.findViewById(R.id.trainingModeSwitch);
        switches = new ArrayList<>();
        switches.add(colorBlindSwitch);
        switches.add(highContrastSwitch);
        switches.add(trainingModeSwitch);
        for(int i = 0; i < settingsOptions.length; i++){
            switches.get(i).setChecked(settingsOptions[i]);
        }
    }

    private void updateView(){
        for(int i = 0; i < switches.size(); i++){
            if(i == selectedIndex){
                switches.get(i).setBackgroundColor(getResources().getColor(R.color.selected));
            }
            else{
                switches.get(i).setBackgroundColor(getResources().getColor(R.color.background));
            }
        }
        scrollView.scrollTo(0, switches.get(selectedIndex).getTop());
        mCardScroller.getAdapter().notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        mView = super.getView(getLayoutInflater().inflate(R.layout.settings_layout, null));

        mGestureDetector = createGestureDetector(this);

        mCardScroller = new CardScrollView(this);
        mCardScroller.setAdapter(new CardScrollAdapter() {
            @Override
            public int getCount() {
                return 1;
            }

            @Override
            public Object getItem(int position) {
                return mView;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return mView;
            }

            @Override
            public int getPosition(Object item) {
                if (mView.equals(item)) {
                    return 0;
                }
                return AdapterView.INVALID_POSITION;
            }
        });
        // Handle the TAP event.
        mCardScroller.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switches.get(selectedIndex).toggle();
                settingsOptions[selectedIndex] = !settingsOptions[selectedIndex];
                AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                am.playSoundEffect(Sounds.TAP);
            }
        });
        setContentView(mCardScroller);

        initViews();
        updateView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCardScroller.activate();
    }

    @Override
    protected void onPause() {
        mCardScroller.deactivate();
        super.onPause();
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event){
        if(mGestureDetector != null){
            return mGestureDetector.onMotionEvent(event);
        }
        return false;
    }

    private GestureDetector createGestureDetector(Context context){
        GestureDetector gestureDetector = new GestureDetector(context);

        gestureDetector.setBaseListener(new GestureDetector.BaseListener() {
            @Override
            public boolean onGesture(Gesture gesture) {
                if (gesture == Gesture.SWIPE_RIGHT) {
                    forwardSwipe();
                    return true;
                } else if (gesture == Gesture.SWIPE_LEFT) {
                    backSwipe();
                    return true;
                } else if (gesture == Gesture.SWIPE_DOWN) {
                    downSwipe();
                    return true;
                }
                return false;
            }
        });
        return gestureDetector;
    }

    private void forwardSwipe(){
        selectedIndex++;
        if(selectedIndex >= switches.size()){
            selectedIndex = 0;
        }
        updateView();
    }

    private void backSwipe(){
        selectedIndex--;
        if(selectedIndex < 0){
            selectedIndex = switches.size() - 1;
        }
        updateView();
    }

    private void downSwipe(){
        Intent intent = new Intent(this, LandingPageActivity.class);
        startActivity(intent);
        finish();
    }
}
