package com.gradproject.bris.dhs.googleglassgradproject.Activities;

import com.google.android.glass.media.Sounds;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.google.android.glass.view.WindowUtils;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;
import com.gradproject.bris.dhs.googleglassgradproject.ActionEvent;
import com.gradproject.bris.dhs.googleglassgradproject.Constants;
import com.gradproject.bris.dhs.googleglassgradproject.Flow;
import com.gradproject.bris.dhs.googleglassgradproject.MasterProgram;
import com.gradproject.bris.dhs.googleglassgradproject.Message;
import com.gradproject.bris.dhs.googleglassgradproject.R;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class GuidedProcedureActivity extends JabberwockyActivity {

    public static List<Flow> medicareFlowList = new ArrayList<Flow>();
    public static List<Flow> centrelinkFlowList = new ArrayList<Flow>();
    public static List<Flow> atoFlowList = new ArrayList<Flow>();

    private int screenID = 0;           // 0 = Select Master P    1 = select gp    2 = GP
    private int selectedProgramId = 0;  // 0 = Medicare     1 = Centrelink      2 = ATO

    private TextView medicareTV, centrelinkTV, atoTV;

    private TextView chooseFlowHeadingTV, pageNumberTV;
    private TextView flow1TV, flow2TV, flow3TV, flow4TV;
    private ArrayList<TextView> textViewsList = new ArrayList<>();

    private LinearLayout yes_ll, back_ll, no_ll;
    private TextView title_tv, time_tv;

    private MasterProgram masterProgram;

    private Flow currentFlow;

    /////////// Timer Stuff ////////////
    private long startTime = 0;
    private String timeStamp = "0:00";
    private Handler timerHandler = new Handler();
    private Runnable timerRunnable = new Runnable(){

        @Override
        public void run() {
            long millis = System.currentTimeMillis() - startTime;
            int seconds = (int) (millis / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;
            timeStamp = String.format("%d:%02d", minutes, seconds);
            timerHandler.postDelayed(this, 0);
            time_tv.setText(timeStamp);
        }
    };
    ///////////////////////////////////

    private CardScrollView mCardScroller;
    private View mView;
    private GestureDetector mGestureDetector;

    private TextView trainingChooseFlowTV;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        populateFlowLists();
        getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);
        mGestureDetector = createGestureDetector(this);

        mView = super.getView(getLayoutInflater().inflate(R.layout.gp_master_program_selection_layout, null));

        mCardScroller = new CardScrollView(this);
        mCardScroller.setAdapter(new CardScrollAdapter() {
            @Override
            public int getCount() {
                return 1;
            }

            @Override
            public Object getItem(int position) {
                return mView;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return mView;
            }

            @Override
            public int getPosition(Object item) {
                if (mView.equals(item)) {
                    return 0;
                }
                return AdapterView.INVALID_POSITION;
            }
        });
        // Handle the TAP event.
        mCardScroller.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(popupDisplay){
                    dismissPopup();
                    return;
                }
                if (screenID == 0) {
                    chooseProgram(selectedProgramId);
                }
                else if (screenID == 1){
                    if(masterProgram.getFlows().isEmpty()){
                        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        am.playSoundEffect(Sounds.DISALLOWED);
                        return;
                    }
                    chooseFlow();
                }
                AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                am.playSoundEffect(Sounds.TAP);
            }
        });
        setContentView(mCardScroller);

        initViewsSelectProgram();
        setSelectedProgram(selectedProgramId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCardScroller.activate();
    }

    @Override
    protected void onPause() {
        mCardScroller.deactivate();
        super.onPause();
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event){
        if(mGestureDetector != null){
            return mGestureDetector.onMotionEvent(event);
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == Constants.SPEECH_REQUEST && resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String spokenText = results.get(0);
            if(spokenText == null) {
                spokenText = "";
            }
            boolean found = false;
            for(int i = 0; i < masterProgram.getFlows().size(); i++){
                if(spokenText.toUpperCase().equals(masterProgram.getFlows().get(i).getName().toUpperCase())){
                    masterProgram.setSelectedIndex(i % 4);
                    masterProgram.setSelectedPage(i / 4);
                    found = true;
                    break;
                }
            }
            if(found == false){
                trainingChooseFlowTV.setText(spokenText + " does not exist!");
                return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
        chooseFlow();
    }

    private GestureDetector createGestureDetector(Context context){
        GestureDetector gestureDetector = new GestureDetector(context);

        gestureDetector.setBaseListener(new GestureDetector.BaseListener() {
            @Override
            public boolean onGesture(Gesture gesture) {
                if(popupDisplay){
                    return false;
                }
                if (gesture == Gesture.SWIPE_RIGHT) {
                    forwardSwipe();
                    return true;
                } else if (gesture == Gesture.SWIPE_LEFT) {
                    backSwipe();
                    return true;
                } else if (gesture == Gesture.SWIPE_UP) {
                    upSwipe();
                    return true;
                } else if (gesture == Gesture.SWIPE_DOWN) {
                    downSwipe();
                    return true;
                } else if (gesture == Gesture.TWO_TAP){
                    twoTap();
                    return true;
                } else if (gesture == Gesture.THREE_TAP){
                    if(popupDisplay){
                        dismissPopup();
                    }else{
                        Message msg = new Message("Joe Adamms", "12:01pm", "Hello, just a reminder that we have a meeting later this afternoon.");
                        displayPopupMessage(msg);
                    }
                    return true;
                }
                return false;
            }
        });
        return gestureDetector;
    }

    private void initViewsSelectProgram(){
        medicareTV = (TextView) mView.findViewById(R.id.medicare_tv);
        centrelinkTV = (TextView) mView.findViewById(R.id.centrelink_tv);
        atoTV = (TextView) mView.findViewById(R.id.ato_tv);
    }
    
    private void initViewsSelectGP(){
        chooseFlowHeadingTV = (TextView) mView.findViewById(R.id.heading_tv);
        flow1TV = (TextView) mView.findViewById(R.id.flow1_tv);
        flow2TV = (TextView) mView.findViewById(R.id.flow2_tv);
        flow3TV = (TextView) mView.findViewById(R.id.flow3_tv);
        flow4TV = (TextView) mView.findViewById(R.id.flow4_tv);
        textViewsList.clear();

        textViewsList.add(flow1TV);
        textViewsList.add(flow2TV);
        textViewsList.add(flow3TV);
        textViewsList.add(flow4TV);
        pageNumberTV = (TextView) mView.findViewById(R.id.page_tv);
        /*trainingChooseFlowTV = (TextView) mView.findViewById(R.id.training_twotap_tv);
        if(!settingsOptions[2]){            // If training mode is off
            trainingChooseFlowTV.setVisibility(View.INVISIBLE);
        }
        trainingChooseFlowTV.setText("Two finger tap to voice search");*/
    }

    private void initViewsGP(){
        yes_ll = (LinearLayout) mView.findViewById(R.id.swipe_forward_ll);
        back_ll= (LinearLayout) mView.findViewById(R.id.swipe_upward_ll);
        no_ll= (LinearLayout) mView.findViewById(R.id.swipe_backward_ll);
        title_tv = (TextView) mView.findViewById(R.id.flow_title_tv);
        time_tv = (TextView) mView.findViewById(R.id.timer_tv);
    }

    private void forwardSwipe(){
        if(screenID == 0){
            selectedProgramId++;
            if(selectedProgramId > 2)   selectedProgramId = 0;
            setSelectedProgram(selectedProgramId);
        }
        else if(screenID == 1){
            masterProgram.setSelectedIndex(masterProgram.getSelectedIndex() + 1);
            if((masterProgram.getSelectedIndex() == (masterProgram.getNumFlowsOnPage())) &&
                    (masterProgram.getSelectedPage() < (masterProgram.getNumPages() - 1))){
                masterProgram.setSelectedIndex(0);
                masterProgram.setSelectedPage(masterProgram.getSelectedPage() + 1);
            }
            if((masterProgram.getSelectedIndex() == (masterProgram.getNumFlowsOnPage())) &&
                    (masterProgram.getSelectedPage() == (masterProgram.getNumPages() - 1))){
                masterProgram.setSelectedIndex(0);
                masterProgram.setSelectedPage(0);
            }

            updateView(masterProgram.getSelectedPage(), masterProgram.getSelectedIndex());
            mCardScroller.getAdapter().notifyDataSetChanged();
        }
        else if(screenID == 2){
            ActionEvent currentEvt = currentFlow.getById(currentFlow.getCurrentId());
            if(currentEvt.getYesChoice().equals(Constants.END_OF_PROCEDURE)){
                screenID = 0;
                mView = super.getView(getLayoutInflater().inflate(R.layout.gp_master_program_selection_layout, null));
                mCardScroller.getAdapter().notifyDataSetChanged();
                initViewsSelectProgram();
                setSelectedProgram(selectedProgramId);
                return;
                //Intent intent = new Intent(this, GuidedProcedureActivity.class);
                //startActivity(intent);
                //finish();
            }
            currentFlow.setCurrentId(currentEvt.getYesChoice());
            ActionEvent newEvt = currentFlow.getById(currentFlow.getCurrentId());
            updateFlowView(newEvt);
        }
    }

    private void backSwipe(){
        if(screenID == 0){
            selectedProgramId--;
            if(selectedProgramId < 0)   selectedProgramId = 2;
            setSelectedProgram(selectedProgramId);
        }
        else if(screenID == 1){
            masterProgram.setSelectedIndex(masterProgram.getSelectedIndex() - 1);
            if((masterProgram.getSelectedIndex() < 0) && (masterProgram.getSelectedPage() > 0)){
                masterProgram.setSelectedPage(masterProgram.getSelectedPage() - 1);
                masterProgram.setSelectedIndex(masterProgram.getNumFlowsOnPage() - 1);
            }
            if((masterProgram.getSelectedIndex() < 0) && (masterProgram.getSelectedPage() == 0)){
                masterProgram.setSelectedPage(masterProgram.getNumPages() - 1);
                masterProgram.setSelectedIndex(masterProgram.getNumFlowsOnPage() - 1);
            }

            updateView(masterProgram.getSelectedPage(), masterProgram.getSelectedIndex());
            mCardScroller.getAdapter().notifyDataSetChanged();
        }
        else if(screenID == 2){
            ActionEvent currentEvt = currentFlow.getById(currentFlow.getCurrentId());
            if(currentEvt.getNoChoice().equals(Constants.INVALID_CHOICE)){
                return;
            }
            currentFlow.setCurrentId(currentEvt.getNoChoice());
            ActionEvent newEvt = currentFlow.getById(currentFlow.getCurrentId());
            updateFlowView(newEvt);
        }
    }

    private void upSwipe(){
        Intent intent = new Intent(this, LandingPageActivity.class);
        startActivity(intent);
        finish();
    }

    private void downSwipe(){
        if(screenID == 0){
            upSwipe();
        }
        else if(screenID == 1){
            screenID = 0;
            mView = super.getView(getLayoutInflater().inflate(R.layout.gp_master_program_selection_layout, null));
            mCardScroller.getAdapter().notifyDataSetChanged();
            initViewsSelectProgram();
            setSelectedProgram(selectedProgramId);
        }
        else if(screenID == 2){
            if(currentFlow.getById(currentFlow.getCurrentId()).getParent().equals(Constants.HOME)){
                chooseProgram(selectedProgramId);
            }
            else{
                currentFlow.setCurrentId(currentFlow.getById(currentFlow.getCurrentId()).getParent());
                ActionEvent newEvt = currentFlow.getById(currentFlow.getCurrentId());
                updateFlowView(newEvt);
            }
        }
    }

    private void twoTap(){
        if(screenID == 1){
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            startActivityForResult(intent, Constants.SPEECH_REQUEST);
        }
    }

    /**
     *  CHOOSE MASTER PROGRAM METHODS
     */

    private void chooseProgram(int programId){
        switch(programId){
            case 0:
                masterProgram = new MasterProgram(Constants.MEDICARE, medicareFlowList);
                break;
            case 1:
                masterProgram = new MasterProgram(Constants.CENTRELINK, centrelinkFlowList);
                break;
            case 2:
                masterProgram = new MasterProgram(Constants.ATO, atoFlowList);
                break;
        }
        mView = super.getView(getLayoutInflater().inflate(R.layout.gp_workflow_selection_layout, null));
        mCardScroller.getAdapter().notifyDataSetChanged();
        screenID = 1;           // Choose GP screen
        initViewsSelectGP();
        chooseFlowHeadingTV.setText(masterProgram.getName() + " - Procedures");
        updateView(masterProgram.getSelectedPage(), masterProgram.getSelectedIndex());
    }

    private void setSelectedProgram(int programId){
        selectedProgramId = programId;
        switch(programId){
            case 0:
                medicareTV.setBackgroundColor(getResources().getColor(R.color.selected));
                centrelinkTV.setBackgroundColor(Color.TRANSPARENT);
                atoTV.setBackgroundColor(Color.TRANSPARENT);
                break;
            case 1:
                medicareTV.setBackgroundColor(Color.TRANSPARENT);
                centrelinkTV.setBackgroundColor(getResources().getColor(R.color.selected));
                atoTV.setBackgroundColor(Color.TRANSPARENT);
                break;
            case 2:
                medicareTV.setBackgroundColor(Color.TRANSPARENT);
                centrelinkTV.setBackgroundColor(Color.TRANSPARENT);
                atoTV.setBackgroundColor(getResources().getColor(R.color.selected));
                break;
            default:
                break;
        }
    }

    /**
     *  CHOOSE GUIDED PROCEDURE METHODS
     */

    private void updateView(int pageNum, int selectedIndex){
        for(int i = 0; i < masterProgram.getFlowGroupList().get(pageNum).getNumFlows(); i++){
            textViewsList.get(i).setText(masterProgram.getFlowGroupList().get(pageNum).getFlow(i).getName());
            textViewsList.get(i).setVisibility(View.VISIBLE);
        }
        for(int j = masterProgram.getFlowGroupList().get(pageNum).getNumFlows(); j < 4; j++){
            textViewsList.get(j).setVisibility(View.INVISIBLE);
        }

        for(int i = 0; i < masterProgram.getFlowGroupList().get(pageNum).getNumFlows(); i++){
            if(i == selectedIndex){
                textViewsList.get(i).setBackgroundColor(getResources().getColor(R.color.selected));
            }
            else{
                textViewsList.get(i).setBackgroundColor(Color.TRANSPARENT);
            }
        }
        pageNumberTV.setText("Page " + (pageNum + 1) + " / " + "Page " + masterProgram.getNumPages());
    }

    private void chooseFlow(){
        screenID = 2;
        mView = super.getView(getLayoutInflater().inflate(R.layout.gp_workflow_layout, null));
        mCardScroller.getAdapter().notifyDataSetChanged();
        initViewsGP();
        currentFlow = masterProgram.getFlowGroupList().get(masterProgram.getSelectedPage()).getFlow(masterProgram.getSelectedIndex());
        updateFlowView(currentFlow.getById(currentFlow.getCurrentId()));
        startTime = System.currentTimeMillis(); // Timer
        timerHandler.postDelayed(timerRunnable, 0);
    }

    /**
     *  GUIDED PROCEDURE METHODS
     */

    private void updateFlowView(ActionEvent evt){
        title_tv.setText(evt.getTitle());
        if(evt.hasNoOption()){
            no_ll.setVisibility(View.VISIBLE);
        }else{
            no_ll.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * THIS IS A TEMPORARY HARDEDCODED METHOD USED FOR TESTING AND DEMONSTRATION PURPOSES
     */
    public static void populateFlowLists(){
        medicareFlowList.clear();
        List<ActionEvent> actionEvents = new ArrayList<ActionEvent>() {{
            add(new ActionEvent("0", "Expired Medicare Card", "H", "1", "N"));
            add(new ActionEvent("1", "Ask if customer has a MyGov account", "0", "2", "3"));
            add(new ActionEvent("2", "Check Linked Services", "1", "4", "N"));
            add(new ActionEvent("3", "Check customer has access to email", "1", "5", "6"));
            add(new ActionEvent("4", "Book into SMSA", "2", "9", "N"));
            add(new ActionEvent("5", "Check does customer have ID", "3", "7", "N"));
            add(new ActionEvent("6", "Book in on iPad", "3", "8", "N"));
            add(new ActionEvent("7", "Book into SMSA", "5", "9", "N"));
            add(new ActionEvent("8", "Send to Face 2 Face", "6", "9", "N"));
            add(new ActionEvent("9", "End of procedure", "8", "E", "N"));
        }};
        Flow expired_medicare_flow = new Flow(actionEvents);

        actionEvents = new ArrayList<ActionEvent>() {{
            add(new ActionEvent("0", "Generic Flow", "H", "1", "N"));
            add(new ActionEvent("1", "Looking to buy a phone?", "0", "2", "7"));
            add(new ActionEvent("2", "Android?", "1", "3", "5"));
            add(new ActionEvent("3", "Point Customer to Samsung Store", "2", "4", "N"));
            add(new ActionEvent("4", "End of procedure", "3", "E", "N"));
            add(new ActionEvent("5", "Point Customer to Apple Store", "2", "6", "N"));
            add(new ActionEvent("6", "End of procedure", "6", "E", "N"));
            add(new ActionEvent("7", "Sorry we only sell phones here", "1", "8", "N"));
            add(new ActionEvent("8", "End of procedure", "7", "E", "N"));
        }};
        Flow generic_flow = new Flow(actionEvents);

        actionEvents = new ArrayList<ActionEvent>() {{
            add(new ActionEvent("0", "Brisbane Flow", "H", "1", "N"));
            add(new ActionEvent("1", "Looking to buy a phone?", "0", "2", "2"));
            add(new ActionEvent("2", "End of procedure", "1", "E", "N"));
        }};
        Flow generic_flow2 = new Flow(actionEvents);
        actionEvents = new ArrayList<ActionEvent>() {{
            add(new ActionEvent("0", "Medicare Refund", "H", "1", "N"));
            add(new ActionEvent("1", "Looking to buy a phone?", "0", "2", "2"));
            add(new ActionEvent("2", "End of procedure", "1", "E", "N"));
        }};
        Flow generic_flow3 = new Flow(actionEvents);
        actionEvents = new ArrayList<ActionEvent>() {{
            add(new ActionEvent("0", "Password Reset", "H", "1", "N"));
            add(new ActionEvent("1", "Looking to buy a phone?", "0", "2", "2"));
            add(new ActionEvent("2", "End of procedure", "1", "E", "N"));
        }};
        Flow generic_flow4 = new Flow(actionEvents);
        actionEvents = new ArrayList<ActionEvent>() {{
            add(new ActionEvent("0", "Username Reset", "H", "1", "N"));
            add(new ActionEvent("1", "Looking to buy a phone?", "0", "2", "2"));
            add(new ActionEvent("2", "End of procedure", "1", "E", "N"));
        }};
        Flow generic_flow5 = new Flow(actionEvents);
        actionEvents = new ArrayList<ActionEvent>() {{
            add(new ActionEvent("0", "I want to win", "H", "1", "N"));
            add(new ActionEvent("1", "Looking to buy a phone?", "0", "2", "2"));
            add(new ActionEvent("2", "End of procedure", "1", "E", "N"));
        }};
        Flow generic_flow6 = new Flow(actionEvents);
        actionEvents = new ArrayList<ActionEvent>() {{
            add(new ActionEvent("0", "The Sunshine State", "H", "1", "N"));
            add(new ActionEvent("1", "Looking to buy a phone?", "0", "2", "2"));
            add(new ActionEvent("2", "End of procedure", "1", "E", "N"));
        }};
        Flow generic_flow7 = new Flow(actionEvents);

        medicareFlowList.add(expired_medicare_flow);
        medicareFlowList.add(generic_flow);
        medicareFlowList.add(generic_flow2);
        medicareFlowList.add(generic_flow3);
        medicareFlowList.add(generic_flow4);
        medicareFlowList.add(generic_flow5);
        medicareFlowList.add(generic_flow6);
        medicareFlowList.add(generic_flow7);

        actionEvents = new ArrayList<ActionEvent>() {{
            add(new ActionEvent("0", "Tax File Number", "H", "1", "N"));
            add(new ActionEvent("1", "Applying for tax file number", "0", "2", "3"));
            add(new ActionEvent("2", "TFN can be applied online via ATO website", "1", "3", "N"));
            add(new ActionEvent("3", "End of procedure", "E", "E", "N"));
        }};
        Flow ato_flow = new Flow(actionEvents);
        atoFlowList.add(ato_flow);

    }

}
